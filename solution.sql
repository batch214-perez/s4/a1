-- s4-a1

-- 1.a.
-- Find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";


-- 1.b.
-- Find all songs that has a length of less than 230.
SELECT * FROM songs WHERE length < 230;


-- 1.c.
-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)

SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;


-- 1.d.
-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";


-- 1.e.
--  Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;


-- 1.f.
-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)

SELECT * FROM (albums  
	JOIN songs ON albums.id = songs.album_id) ORDER BY album_title DESC;


